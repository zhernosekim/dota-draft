<?php

use helpers\Db;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

spl_autoload_register(function ($className) {
	require  $className . '.php';
});

Db::$host = 'localhost';
Db::$user = 'root';
Db::$password = 'qweasd';
Db::$dbName = 'draft';
Db::getInstance();


if (!empty($_GET['id'])) {
	$game = \classes\Game::parseId($_GET['id']);
	$game->printScores();
} elseif (!empty($_GET['url'])) {
	\classes\Matches::getByDotabuffUrl($_GET['url']);
} elseif (!empty($_GET['analyze'])) {
	new \classes\Analyze();
}
else {
	$game = new \classes\Game();
	$game->addHeroByNameToFirstTeam('Gyro');
	$game->addHeroByNameToFirstTeam('Doom');
	$game->addHeroByNameToFirstTeam('Dragon');
	$game->addHeroByNameToFirstTeam('Lion');
	$game->addHeroByNameToFirstTeam('Pangolier');

	$game->addHeroByNameToSecondTeam('Io');
	$game->addHeroByNameToSecondTeam('Luna');
	$game->addHeroByNameToSecondTeam('Ancie');
	$game->addHeroByNameToSecondTeam('Batrider');
	$game->addHeroByNameToSecondTeam('Razor');

	$game->calculate();
	$game->printScores();
}