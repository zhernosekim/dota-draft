<?php

namespace traits;

trait ParseHelper {
	protected function getAValue($text) {
		preg_match('@<a.*>(.*)</a>@msU', $text, $match);
		if (empty($match)) {
			return null;
		};

		return $match[1];
	}

	protected function getAHrefValue($text) {
		preg_match('@<a.*href="(.*)"@msU', $text, $match);
		return $match[1];
	}

	protected function getMedianFromSortedArray($array) {
		$array_total = sizeof($array);
		if ($array_total % 2 === 1) {
			return [$array[floor($array_total / 2)], $array[floor($array_total / 2)]];
		} else {
			return [$array[$array_total / 2 - 1], $array[$array_total / 2 + 1]];
		}
	}

	protected function getDigits($text) {
		return preg_replace('@\D@msU', '', $text);
	}
}