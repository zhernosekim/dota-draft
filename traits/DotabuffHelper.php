<?php

namespace traits;

trait DotabuffHelper {
	use ParseHelper;

	protected function getDomain() {
		return 'https://www.dotabuff.com/';
	}

	protected function getNumberOfPages($text) {
		if (strpos($text, 'last') === false) {
			return 1;
		};

		preg_match('@<span class="last">(.*)</span>@msU', $text, $match);
		preg_match('@page=(.*)"@msU', $match[1], $result);

		return $result[1];
	}

	protected function getLeagueId($url) {
		$explodes = explode('/', $url);
		$index = array_search('leagues', $explodes);
		$explodes = explode('-', $explodes[$index + 1]);

		return reset($explodes);
	}

	protected function getUrl($text) {
		preg_match('@<span class="last">(.*)</span>@msU', $text, $match);
		$raw_url = $this->getAHrefValue($match[1]);
		$explodes = explode('&', $raw_url);
		array_pop($explodes);

		return $this->getDomain() . implode('&', $explodes);
	}
}