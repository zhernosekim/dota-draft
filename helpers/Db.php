<?php

namespace helpers;

class Db extends Singleton
{
    public static $mysqli;
    public static $host;
    public static $user;
    public static $password;
    public static $dbName;


    static function customConstruct($instance)
    {
        self::$mysqli = new \mysqli(self::$host, self::$user, self::$password, self::$dbName);
        $mysqli = self::$mysqli;

        if ($mysqli->connect_error) {
            die('Ошибка подключения (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
        };
    }

    public static function query ($query)
    {
        if (empty(self::$mysqli)) {
            self::getInstance();
        };

        $result = self::$mysqli->query($query);
        if (!$result) {
            throw new \Exception( self::$mysqli->error . '. Query: "' . $query . '"');
        };

        return $result;
    }

    public static function fetchAssoc($query, $key = null)
    {
        $res = self::query($query);
        $result = [];
        if ($res) {
            while ( $row = $res->fetch_assoc()) {
                if (is_null($key)) {
                    $result[] = $row;
                } else {
                    $result[$row[$key]] = $row;
                }
            }
            $res->free();
        };
        return $result;
    }

    public static function fetchOne($query)
	{
    	$result = self::fetchAssoc($query);
    	return reset($result);
	}

    public static function realEscapeString($string)
    {
        return mysqli_real_escape_string(self::$mysqli, $string);
    }

    public static function getLastInsertedId() {
		return mysqli_insert_id(self::$mysqli);
	}

    public static function createTableAdvantages() {
    	self::query("CREATE TABLE `advantages` (
			 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			 `main_hero` varchar(20) NOT NULL,
			 `second_hero` varchar(20) NOT NULL,
			 `score` double NOT NULL,
			 PRIMARY KEY (`id`),
			 KEY `main_hero` (`main_hero`)
			) ENGINE=InnoDB");
	}

    public static function createTableGames() {
    	self::query("	
			CREATE TABLE `games` (
			 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			 `external_id` int(10) unsigned NOT NULL,
			 `first_team_name` varchar(50) NOT NULL,
			 `second_team_name` varchar(50) NOT NULL,
			 `series_id` int(11) DEFAULT NULL,
			 `league_id` int(11) DEFAULT NULL,
			 `won` tinyint(1) DEFAULT NULL,
			 `predicted` tinyint(1) DEFAULT NULL,
			 `playoff` tinyint(1) DEFAULT NULL,
			 PRIMARY KEY (`id`),
			 UNIQUE KEY `external_id` (`external_id`)
			) ENGINE=InnoDB DEFAULT
		");

    	self::query("
			CREATE TABLE `games_heroes` (
			 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			 `game_id` int(10) unsigned NOT NULL,
			 `hero_name` varchar(20) NOT NULL,
			 `team` tinyint(1) DEFAULT NULL,
			 PRIMARY KEY (`id`),
			 KEY `first_team_hero` (`hero_name`),
			 KEY `second_team_hero` (`team`),
			 CONSTRAINT `games_heroes_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE=InnoDB
		");
	}
}