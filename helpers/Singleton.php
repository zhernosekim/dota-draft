<?php

namespace helpers;

abstract class Singleton
{
    /**
     * @return static
     */
    final public static function getInstance() : self
    {
        static $instance = null;

        if (null === $instance) {
            $instance = new static();
            static::customConstruct($instance);
        };

        return $instance;
    }

    /**
     * Method on creating
     *
     * @param self $instance
     * @return mixed
     */
    static function customConstruct($instance) {}

    final protected function __clone() {}
    protected function __construct() {}
}