<?php

namespace helpers;

class Curl {
	public static function getPageByUrl($url, $throwError = true) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 7);
		$page = curl_exec($ch);
		curl_close($ch);

		if (empty($page) && $throwError) {
			throw new Exception('Empty page in url ' . $url);
		};

		return $page;
	}
}