<?php

namespace classes;

use helpers\Curl;
use helpers\Db;

class Hero {

	const META = [
//		'Terrorblade' => -3,
	];

	const CUSTOM_ADVANTAGES = [
		[
			'first_hero' => 'Luna',
			'second_hero' => 'Gyrocopter',
			'value' => 1,
		]
	];

	protected $name;
	public $advantages = [];
	/**
	 * @var float
	 */
	public $score;

	protected function __construct() {
	}

	protected function getCountersUrl() {
		$name = str_replace('&#39;', '', strtolower($this->name));
		$name = str_replace(' ', '-', strtolower($name));
		return sprintf('https://www.dotabuff.com/heroes/%s/counters', $name);
	}

	public function processName($name) {
		$name = str_replace('&#39;', '', $name);
		$name = str_replace('Anti Mage', 'Anti-Mage', $name);
		return $name;
	}

	public function setName($name) {
		$this->name = $this->processName($name);
	}

	public function getName() {
		return $this->name;
	}

	public static function createByName($name) {
		$hero = new self();
		$hero->setName($name);
		$hero->setAdvantages();
		return $hero;
	}

	public function setAdvantages() {
		$res = Db::fetchOne("SELECT * FROM `advantages` 
			WHERE `main_hero` = '". Db::realEscapeString($this->name) . "' LIMIT 1");
		if (empty($res)) {
			$res = Db::fetchOne("SELECT * FROM `advantages` 
			WHERE `main_hero` like '%" . Db::realEscapeString($this->name) . "%' LIMIT 1");
		};

		if (empty($res)) {
			$this->getAdvantagesFromDotabuff();
		} else {
			$this->name = $res['main_hero'];
			$advantages = Db::fetchAssoc("
				SELECT * FROM `advantages` 
				WHERE `main_hero`='". Db::realEscapeString($this->name) . "'
				ORDER BY `second_hero` 
			");
			foreach ($advantages as $advantage) {
				$this->advantages[$advantage['second_hero']] = $advantage['score'];
			}
		}
	}

	protected function getAdvantagesFromDotabuff() {
		$page = Curl::getPageByUrl($this->getCountersUrl());
		preg_match_all('@<tr data-link-to.*>(.*)</tr>@msU', $page, $trs);
		if (empty($trs) || sizeof($trs[1]) < 100) {
			throw new \Exception(sprintf('Hero with name %s have not found', $this->name));
		};

		foreach ($trs[1] as $tr) {
			preg_match('@<td.*</td><td.*<a.*>(.*)</a>.*</td><td.*>(.*)%@msU', $tr, $match);
			$score = -1 * (float) $match[2];
			$this->advantages[$match[1]] = $score;
			$second_name = $this->processName($match[1]);
			Db::query("INSERT INTO `advantages` SET 
					main_hero='" . Db::realEscapeString($this->name) . "', 
					second_hero ='" . Db::realEscapeString($second_name)  . "', 
					score = '$score'");
		}
	}

	/**
	 * @param Hero[] $heroes
	 */
	public function setScoreDefault(array $heroes) {
		$this->score = 0;

		foreach ($heroes as $hero) {
			if ($this->name === $hero->name) {
				continue;
			};

			$advantage = $this->advantages[$hero->name];
			$this->score += $advantage;
		}
	}

	/**
	 * @param Hero[] $heroes
	 */
	public function setScoreMeta(array $heroes) {
		$this->score = 0;

		foreach ($heroes as $hero) {
			if ($this->name === $hero->name) {
				continue;
			};

			$advantage = $this->advantages[$hero->name];
			if (!empty(self::META[$hero->name])) {
				$advantage -= self::META[$hero->name];
			};
			foreach (self::CUSTOM_ADVANTAGES as $item) {
				if ($this->name === $item['first_hero'] && $hero->name === $item['second_hero']) {
					$advantage += $item['value'];
				} elseif ($this->name === $item['second_hero'] && $hero->name === $item['first_hero']) {
					$advantage -= $item['value'];
				}
			}
			$this->score += $advantage;
		}
	}
}