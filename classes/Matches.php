<?php

namespace classes;

use helpers\Curl;

class Matches {
	use \traits\DotabuffHelper;

	const SCORES_DIFFERENCE_TO_PRINT = 4;

	public $ids = [];
	public $url = '';
	public $league_id = null;
	/**
	 * @var Game[]
	 */
	public $games = [];
	public $additional_stastics = [];

	public function __construct() {
	}

	public static function getByDotabuffUrl($url) {
		$matches = new self();
		$page = Curl::getPageByUrl($url);
	    if (self::urlHasMatches($url)) {
	        $matches->createFromDotabuffMatches($page, $url);
        } else {
			preg_match_all('@<a href="/matches/(.*)"@msU', $page, $result);
			$matches->ids = $result[1];
			foreach ($result[1] as $id) {
				$matches->games[] = Game::parseId($id);
			}
		}

		$matches->calculateAndPrint();
	}

	protected static function urlHasMatches($url) : bool {
        return (bool) strpos($url, 'matches');
    }

    protected function createFromDotabuffMatches($page, $url) {
	    $number_of_pages = $this->getNumberOfPages($page);
	    $this->url = $number_of_pages > 1 ? $this->getUrl($page) : $url;
	    $this->league_id = $this->getLeagueId($this->url);

	    $i = 1;
	    while ($i <= $number_of_pages && $i < 10) {
	        $this->addGamesFromPage($page);

			if ($i < $number_of_pages) {
				$page = Curl::getPageByUrl($this->url . '&page=' . ($i + 1));
			}
			$i++;
		}
    }

    protected function calculateAndPrint() {
		foreach ($this->games as $game) {
			$game->calculate();
		}
		$this->displayInfo();

		foreach ($this->games as $game) {
			$game->calculate('setScoreMeta');
		}
		$this->displayInfo();
    }

    protected function addGamesFromPage($page) {
		preg_match('@recent-esports-matches(.*)</table>@msU', $page, $result);
		preg_match_all('@<tr.*>(.*)</tr>@msU', $result[1], $trs);
		array_shift($trs[1]);
		foreach ($trs[1] as $tr) {
			$game = new Game();
			$this->games[] = $game;
			preg_match_all('@<td.*>(.*)</td>@msU', $tr, $tds);
			$game->won = 1;
			$game->league_id = $this->league_id;
			$game->id = $this->getAValue($tds[1][0]);
			$game->first_team_name = $this->getTeamName($tds[1][2]);
			$game->first_team_heroes_names = $this->getTeamHeroes($tds[1][2]);
			$game->second_team_name = $this->getTeamName($tds[1][3]);
			$game->second_team_heroes_names = $this->getTeamHeroes($tds[1][3]);
			$game->series_id = $this->getSeriesId($tds[1][1]);
		}
    }

    protected function getSeriesId($text) {
	    $result = $this->getAValue($text);
	    return $this->getDigits($result);
    }

    protected function getTeamName($text) {
	    preg_match('@team-text-full">(.*)</span>@msU', $text, $result);
	    return $result[1];
    }

    protected function getTeamHeroes($text) {
	    $names = [];
	    preg_match_all('@<img class="img-icon" src="(.*)"@msU', $text, $result);
		foreach ($result[1] as $item) {
		    $item = html_entity_decode($item);
            $explodes = explode('/', $item);
            $explodes = explode('-', array_pop($explodes));
			array_pop($explodes);
			foreach ($explodes as &$explode) {
			    $explode = strtoupper(substr($explode, 0, 1)) . substr($explode, 1);
			}

			$names[] = implode(' ', $explodes);
	    }

	    return $names;
    }

    protected function prePrint() {
	    $games = array_filter($this->games, function ($game) {
			return $game->scores_difference < self::SCORES_DIFFERENCE_TO_PRINT;
        });
	    $this->setAdditionalStatistics($this->games);
		$this->sortGamesByScoresDiff($games);
		foreach ($games as $game) {
            echo $game->id , ', ';
		}
echo '<br>';
	    return $games;
    }

	/**
	 * @param Game[] $games
	 */
	protected function setAdditionalStatistics($games) {
	    $games_playoff = [];
	    $games_groupstage = [];

		foreach ($games as $game) {
            if ($game->id < 3813737784) {
                $games_groupstage[] = $game;
            } else {
                $games_playoff[] = $game;
            }
	    }

		$games_0_3 = [];
		$games_3_6 = [];
		$games_6_9 = [];
		$games_9_12 = [];
		$games_12_99 = [];
		foreach ($games as $game) {
			if ($game->scores_difference >= 0 && $game->scores_difference < 3) {
				$games_0_3[] = $game;
			} elseif ($game->scores_difference >= 3 && $game->scores_difference < 6) {
				$games_3_6[] = $game;
			} elseif ($game->scores_difference >= 6 && $game->scores_difference < 9) {
				$games_6_9[] = $game;
			} elseif ($game->scores_difference >= 9 && $game->scores_difference < 12) {
				$games_9_12[] = $game;
			} elseif ($game->scores_difference >= 12) {
				$games_12_99[] = $game;
			}
		}

	    $this->additional_stastics = [
            'total' => $this->getStatistics($games),
            'playoff' => $this->getStatistics($games_playoff),
            'groupstage' => $this->getStatistics($games_groupstage),
            '' => null,
            'games_0_3' => $this->getStatistics($games_0_3),
            'games_3_6' => $this->getStatistics($games_3_6),
            'games_6_9' => $this->getStatistics($games_6_9),
            'games_9_12' => $this->getStatistics($games_9_12),
            'games_12_99' => $this->getStatistics($games_12_99),
        ];
    }

	/**
	 * @param Game[] $games
	 * @return array
	 */
	protected function getStatistics($games) {
	    $total = sizeof($games);
	    $right = 0;
		foreach ($games as $game) {
            if ($game->first_team_score > $game->second_team_score && $game->won === 1) {
                $right++;
            };
	    }
	    $right_procent = $right / $total * 100;

	    return [
	        'total' => $total,
	        'right' => $right,
	        'right_procent' => $right_procent,
        ];
    }

    protected function printStatistic($stats, $key) {
	    if (empty($stats)) {
	        echo '<br>';
	        return;
        };
	    echo sprintf('<br>%s: %s/%s (%s%%)', $key, $stats['right'], $stats['total'], $stats['right_procent']);
    }

	public function displayInfo() {
	    $games = $this->prePrint();
		echo '<table>';
		foreach ($games as $game) {
			?>
				<tr>
					<td>
						<a href="http://localhost/dota-draft/?id=<?= $game->id ?? '' ?>"><?= $game->id ?? '' ?></a>
					</td>
					<td>
						<?= $game->first_team_name ?>
					</td>
					<td
						<?php
						if ($game->predicted) {
							echo 'style="font-weight: bold;"';
						}
						?>
					>
						<?php
						echo $game->first_team_score; if ($game->won === 1) echo ' Выиграла'
						?>
					</td>
					<td>
						<?= $game->second_team_name ?>
					</td>
					<td
						<?php
						if ($game->predicted) {
							echo 'style="font-weight: bold;"';
						}
						?>
					>
						<?php echo $game->second_team_score; if ($game->won === 2) echo ' Выиграла' ?>
					</td>
                    <td><?= $game->scores_difference ?></td>
				</tr>
			<?php
		}
		echo '</table>';
		foreach ($this->additional_stastics as $key => $additional_stastic) {
            $this->printStatistic($additional_stastic, $key);
		}
		echo '<br>';
		$this->printScoresStatistic();
	}

	protected function printScoresStatistic() {
	    $stats = $this->getScoresStatistic();
	    echo sprintf('<br>min: %s, max: %s, median: %s', $stats['min'], $stats['max'], $stats['median']);
	}
	protected function getScoresStatistic() {
		$games = array_filter($this->games, function ($game) {
		    return $game->scores_difference > 0;
        });


	    $min_scores = 999;
		foreach ($games as $game) {
            if ($game->scores_difference < $min_scores) {
                $min_scores = $game->scores_difference;
            }
	    }

	    $max_scores = 0;
		foreach ($games as $game) {
			if ($game->scores_difference > $max_scores) {
				$max_scores = $game->scores_difference;
			}
		}

		$this->sortGamesByScoresDiff($games);

		$result = $this->getMedianFromSortedArray($games);
		$median = ($result[0]->scores_difference + $result[1]->scores_difference) / 2;

		return [
            'min' => $min_scores,
            'max' => $max_scores,
            'median' => $median,
        ];
    }

    protected function sortGamesByScoresDiff(&$games) {
		usort($games, function ($game1, $game2) {
			return $game1->scores_difference <=> $game2->scores_difference;
		});
    }
}