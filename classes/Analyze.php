<?php

namespace classes;

use helpers\Db;

class Analyze {
	public function __construct() {
		$games = $this->getGames();
		$teams = $this->getTeams($games);
		$stats = $this->getTeamStats($games, $teams);


	}

	protected function getGames() : array {
		/* @var Game[] $games */
		$games = [];
		$rows = Db::fetchAssoc("SELECT * FROM `games`");
		foreach ($rows as $row) {
			$game = new Game();
			$game->id = $row['external_id'];
			$game->first_team_name = $row['first_team_name'];
			$game->second_team_name = $row['second_team_name'];
			$game->series_id = $row['series_id'];
			$game->league_id = $row['league_id'];
			$game->won = $row['won'];
			$game->predicted = $row['predicted'];
			$game->playoff = $row['playoff'];
			$games[$row['id']] = $game;
		}

		$rows = Db::fetchAssoc("SELECT * FROM `games_heroes`");
		foreach ($rows as $row) {
			$hero = Hero::createByName($row['hero_name']);
			if ($row['team'] == 1) {
				$games[$row['game_id']]->first_team_heroes[] = $hero;
			} else {
				$games[$row['game_id']]->second_team_heroes[] = $hero;
			}
		}

		return $games;
	}

	/**
	 * @param Game[] $games
	 * @param array $teams
	 */
	protected function getTeamStats(array $games, array $teams) {
		$stats = [];
		foreach ($teams as $team) {
			$stat = [
				'predicted' => 0,
				'not_predicted' => 0,
			];
			foreach ($games as $game) {
				if ($game->first_team_name !== $team && $game->second_team_name !== $team) {
					continue;
				};

				if ($game->predicted) {
					$stat['predicted']++;
				} elseif ($game->predicted === '0') {
					$stat['not_predicted']++;
				}
			}
			$stats[$team] = $stat;
		}

		return $stats;
	}

	/**
	 * @param Game[] $games
	 * @return array
	 */
	protected function getTeams(array $games) {
		$teams = [];
		foreach ($games as $game) {
			$teams[] = $game->first_team_name;
			$teams[] = $game->second_team_name;
		}

		$teams = array_unique($teams);

		return $teams;
	}
}