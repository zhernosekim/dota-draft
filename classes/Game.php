<?php

namespace classes;

use helpers\Curl;
use helpers\Db;

class Game {
	public $id;
	public $first_team_heroes_names = [];
	public $second_team_heroes_names = [];
	public $won = null;
	public $first_team_name = '';
	public $second_team_name = '';
	public $scores_difference = null;
	public $series_id = null;
	public $league_id = null;
	public $predicted = null;
	public $playoff = null;

	public function __construct($id = null) {
		$this->id = $id;
	}

	/**
	 * @var Hero[]
	 */
	public $first_team_heroes = [];
	/**
	 * @var Hero[]
	 */
	public $second_team_heroes = [];

	public $first_team_score = 0;
	public $second_team_score = 0;

	public function addHeroByNameToFirstTeam($name) {
		if (!empty($name)) {
			$this->first_team_heroes_names[] = $name;
		};
	}

	public function addHeroByNameToSecondTeam($name) {
		if (!empty($name)) {
			$this->second_team_heroes_names[] = $name;
		};
	}

	/**
	 * Получаем героев дотабафа и суммируем их очки преимущества
	 */
	public function calculate($method_name = 'setScoreDefault') {
		$this->first_team_score = 0;
		$this->second_team_score = 0;
		$this->first_team_heroes = [];
		$this->second_team_heroes = [];
		$this->setDotabuffHeroes();

		foreach ($this->first_team_heroes as $hero) {
			$this->first_team_score += $this->getHeroScore($hero, $this->second_team_heroes, $method_name);
		}

		foreach ($this->second_team_heroes as $hero) {
			$this->second_team_score += $this->getHeroScore($hero, $this->first_team_heroes, $method_name);
		}

		$this->setScoresDiff();
		$this->predicted = $this->first_team_score > $this->second_team_score && $this->won === 1;
//		if (!empty($this->id)) {
//			$this->save();
//		};
	}

	protected function getHeroScore(Hero $hero, array $enemy_team_heroes, string $method_name) {
		$hero->$method_name($enemy_team_heroes);
		return $hero->score;
	}

	protected function setDotabuffHeroes() {
		foreach ($this->first_team_heroes_names as $name) {
			$this->first_team_heroes[] = Hero::createByName($name);
		}

		foreach ($this->second_team_heroes_names as $name) {
			$this->second_team_heroes[] = Hero::createByName($name);
		}
	}

	protected function setScoresDiff() {
		$this->scores_difference = abs($this->first_team_score - $this->second_team_score);
	}

	public function printScores() {
		if (!empty($this->first_team_name)) {
			echo sprintf('<b>%s</b><br>', $this->first_team_name);
		};
		foreach ($this->first_team_heroes as $hero) {
			echo sprintf('%s: %s<br>', $hero->getName(), $hero->score);
		}
		echo sprintf('<b>Team1: %s </b>', $this->first_team_score);
		if ($this->won === 1) {
			echo ' - Выиграла';
		};
		echo '<br>';
		echo '<br>';

		if (!empty($this->second_team_name)) {
			echo sprintf('<b>%s</b><br>', $this->second_team_name);
		};
		foreach ($this->second_team_heroes as $hero) {
			echo sprintf('%s: %s<br>', $hero->getName(), $hero->score);
		}
		echo sprintf('<b>Team2: %s </b>', $this->second_team_score);
		if ($this->won === 2) {
			echo ' - Выиграла';
		};
		echo '<br>';
	}

	public static function parseId($id) {
		$game = new self($id);
		$page = Curl::getPageByUrl('https://www.dotabuff.com/matches/' . $id);

		preg_match_all('@<header style="vertical-align: middle">.*team-text-full">(.*)<@msU', $page, $matches);
		$teams = array_unique($matches[1]);
		$game->first_team_name = reset($teams);
		$game->second_team_name = array_pop($teams);

		if (preg_match('@<section class="dire">.*(victory-icon)@msU', $page,$match)) {
			$game->won = 2;
		} else {
			$game->won = 1;
		}

		preg_match_all('@<article class="r-tabbed-table"><table.*>(.*)</article>@msU', $page, $tables);
		foreach ($tables[1] as $table) {
			preg_match_all('@<tr.*>(.*)</tr>@msU', $table, $trs);
			foreach ($trs[1] as $tr) {
				preg_match('@<td.*<img.*title="(.*)"@msU', $tr, $match);
				if (empty($match)) {
					continue;
				};

				if (sizeof($game->first_team_heroes_names) < 5) {
					$game->first_team_heroes_names[] = $match[1];
				} else {
					$game->second_team_heroes_names[] = $match[1];
				}
			}
		}

		$game->calculate();

		return $game;
	}

	public function save() {
		if (sizeof($this->first_team_heroes) !== 5 || sizeof($this->second_team_heroes) !== 5) {
			return false;
		};

		Db::query("INSERT INTO `games` SET		
			  `external_id`='" . Db::realEscapeString($this->id) . "',
			  `first_team_name`='" . Db::realEscapeString($this->first_team_name) . "',
			  `second_team_name`='" . Db::realEscapeString($this->second_team_name) . "',
			  `series_id`='" . Db::realEscapeString($this->series_id) . "',
			  `league_id`='" . Db::realEscapeString($this->league_id) . "',
			  `won`='" . Db::realEscapeString($this->won) . "',
			  `predicted`='" . Db::realEscapeString($this->predicted) . "',
			  `playoff`='" . Db::realEscapeString($this->playoff) . "'					  
		");

		$id = Db::getLastInsertedId();
		if (empty($id)) {
			throw new \Exception('Can not get inserted id');
		}

		foreach ($this->first_team_heroes as $hero) {
			Db::query("INSERT INTO `games_heroes` SET		
			  `game_id`='" . Db::realEscapeString($id) . "',
			  `hero_name`='" . Db::realEscapeString($hero->getName()) . "',
			  `team`='1'			  
			");
		}

		foreach ($this->second_team_heroes as $hero) {
			Db::query("INSERT INTO `games_heroes` SET		
			  `game_id`='" . Db::realEscapeString($id) . "',
			  `hero_name`='" . Db::realEscapeString($hero->getName()) . "',
			  `team`='2'
			");
		}

	}

}